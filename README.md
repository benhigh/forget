# Forget

Created for Global Game Jam 2020 at the SLU site in St. Louis. Will receive some small Quality of Life changes, original release from the jam is linked at the bottom of the page.

## The Story

This game is about repairing your broken heart by erasing painful memories. Traverse challenging platforming levels to help clear your conscience and move on.

## The Theme

The theme for Global Game Jam 2020 was "Repair". We wanted to convey the idea of "repairing" the memories of the character (through memory recall hypnosis) in a way that is desirable to the character but undesirable to the player.

## Credits

Cameron Walker - Design, Audio & Programming

Sarah Lester - Artist, Audio & Programming

Jacob Stauch - Music & Programming

Austin Froese - Programming

Ben High - Programming


## [Legacy Release](https://globalgamejam.org/2020/games/forget-6)