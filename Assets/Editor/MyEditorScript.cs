using System;
using UnityEditor;
using UnityEngine;

public class MyEditorScript : MonoBehaviour {
        static void PerformBuild () {
            string[] scenes = {
                "Assets/Scenes/TitlePage.unity",
                "Assets/Scenes/Intro.unity",
                "Assets/Scenes/Hub.unity",
                "Assets/Scenes/LeftVentricle.unity",
                "Assets/Scenes/LeftAtrium.unity",
                "Assets/Scenes/RightVentricle.unity",
                "Assets/Scenes/RightAtrium.unity",
                "Assets/Scenes/Credits.unity"
            };
            BuildPipeline.BuildPlayer(scenes, "/home/ben/forgetbuilt/linux/forget", BuildTarget.StandaloneLinux64, BuildOptions.None);
            BuildPipeline.BuildPlayer(scenes, "/home/ben/forgetbuilt/windows/forget.exe", BuildTarget.StandaloneWindows64, BuildOptions.None);
            BuildPipeline.BuildPlayer(scenes, "/home/ben/forgetbuilt/osx/forget", BuildTarget.StandaloneOSX, BuildOptions.None);
        }
}