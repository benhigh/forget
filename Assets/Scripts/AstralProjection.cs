﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstralProjection : MonoBehaviour
{
	public GameObject sceneChangeCollider;
	public Vector2 centerOfCollider;

	public LevelManager LM;

	void Awake() {
		LM = FindObjectOfType<LevelManager>();
	}

    // Start is called before the first frame update
    void Start()
    {
        sceneChangeCollider = GameObject.Find("SceneChangingCollider");
		centerOfCollider = sceneChangeCollider.transform.position;
		StartCoroutine(simpleWait());
    }

    // Update is called once per frame
    void Update()
    {
		Vector2 startPos = new Vector2(transform.position.x, transform.position.y);
		this.transform.position = Vector2.MoveTowards(startPos, centerOfCollider, Time.deltaTime * 0.7f);
    }

	IEnumerator simpleWait() {
		yield return new WaitForSeconds(7f);
		Debug.Log("Loading hub");
		StartCoroutine(LM.loadLevel("Hub"));
	}
}
