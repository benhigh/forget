﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorScript : MonoBehaviour {
    public GameObject player;

    public string levelName;

    public int level;
    public string[] levels;

    public GameObject door;

    public LevelManager LM;

    void Awake() {
    	LM = FindObjectOfType<LevelManager>();
    }

    // Start is called before the first frame update
    void Start()
    {

        if (levelName == "Hub") {

            PlayerController.pictureProgress++;
        }

        if (PlayerController.pictureProgress >= level  && levelName != "Hub") {
            print("level met");
            Destroy(door);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.transform.tag == "Player") {
            print("here");
            if (levelName == "Hub") {
				StartCoroutine(LM.loadLevel(levelName));

            }
            else {
				string tmp = levels[PlayerController.pictureProgress];
                StartCoroutine(LM.loadLevel(tmp));
            }
        }
    }
}
