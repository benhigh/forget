﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(ShowCredits());
    }

    private IEnumerator ShowCredits() {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("TitlePage");
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
