﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public Transform target;

    public float leftBound = 0;
    public float rightBound = 100;

    public float yLevel = 0;

    public void Update() {
        Vector3 targetPosition = new Vector3(Mathf.Clamp(target.position.x, leftBound, rightBound), target.position.y, -10);
        transform.position = targetPosition;
    }
}
