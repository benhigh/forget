﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PictureScript : MonoBehaviour
{

    public Sprite[] pictureArray;

    private GameObject player;


    // Start is called before the first frame update
    void Start() {

         player = GameObject.FindGameObjectWithTag("Player");
        //PlayerController playerController = player.GetComponent<PlayerController>();
        print(PlayerController.pictureProgress);
        this.GetComponent<SpriteRenderer>().sprite = pictureArray[PlayerController.pictureProgress];
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.tag == "Player" && PlayerController.pictureProgress >= 4) {
            print("end condition");
            player.GetComponent<PlayerController>().enabled = false;
            StartCoroutine(EndTrigger());
        }
    }

    private IEnumerator EndTrigger() {
    
        yield return new WaitForSeconds(2f);
     
        PlayerController.pictureProgress = 0;
        SceneManager.LoadScene("Credits");
    }


}
