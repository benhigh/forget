﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

// Authors: Ben High, Jacob Stauch
// Better jumping code inspired by Board To Bits Games

public class PlayerController : MonoBehaviour {
	public static int pictureProgress = 0;
    public float moveSpeed = 3f;
    public float jumpPower = 8f;

    public float fallMultiplier = 2.5f;
    public float shortHopMultiplier = 2f;
    
    public float deathBarrier = -20f;

    public PlayerController.Direction facing = PlayerController.Direction.Right;

    public bool jumping;
    public bool jumpRequest;
    public bool isJumpButtonDown;

	public int jumpDir;
	public bool jumpDirChecked;

    public GroundDetector groundDetector;

    private Rigidbody2D _rigidBody;

    public void Start() {
	    Cursor.lockState = CursorLockMode.Locked;
	    Cursor.visible = false;
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    public void Update() {
        // Reset scene if Player falls below a specific y value designated by deathBarrier
        if(this.transform.position.y < deathBarrier)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            
        // Get input values
        float xInput = Input.GetAxis("Horizontal");
        isJumpButtonDown = Input.GetButton("Jump");

        // Check if we're on the ground
        jumping = !groundDetector.IsOnGround();

        // Jump
        if (isJumpButtonDown && Input.GetButtonDown("Jump") && !jumping) {
            jumpRequest = true;
        }

		if (!jumping) {
			jumpDirChecked = false;
			// Set the facing direction
	        if (xInput > 0) {
	            facing = Direction.Right;
	        } else if (xInput < 0) {
	            facing = Direction.Left;
	        }
        	transform.Translate(moveSpeed * Time.deltaTime * xInput, 0, 0);
		}

		// Create a fixed jump arc
		else {
			if (!jumpDirChecked) {
				if (xInput > 0) {
					jumpDir = 1;
				}
				else if (xInput < 0) {
					jumpDir = -1;
				}
				else if (xInput == 0) {
					jumpDir = 0;
				}
				jumpDirChecked = true;
			}
			transform.Translate(moveSpeed * Time.deltaTime * jumpDir, 0, 0);
		}
    }

    public enum Direction {
        Left,
        Right
    }

    void FixedUpdate() {
        if (jumpRequest) {
            //_rigidBody.AddForce(new Vector2(0, jumpPower));
            //_rigidBody.velocity = Vector2.up * jumpPower;
            _rigidBody.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            jumpRequest = false;
        }
        // Apply ground force
        if (_rigidBody.velocity.y < 0) {
          	_rigidBody.velocity += Vector2.up * Physics2D.gravity.y *
          	(fallMultiplier - 1) * Time.deltaTime;
        }
        // Add falling force if letting go of jump in mid air
        else if (_rigidBody.velocity.y > 0 && !isJumpButtonDown && jumping) {
            _rigidBody.velocity += Vector2.up * Physics2D.gravity.y *
            (shortHopMultiplier - 1) * Time.deltaTime;
        }
    }
}
