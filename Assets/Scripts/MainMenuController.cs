﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    public TMP_Text[] menuTexts;
    public int selection = 0;

    public float yLastFrame = 0.0f;

    public float axisThreshold = 0.1f;

    public LevelManager LM;

    private void Awake() {
        LM = FindObjectOfType<LevelManager>();
    }

    private void Update() {
        if (Input.GetAxis("Vertical") > axisThreshold && yLastFrame <= axisThreshold) {
            selection--;
            if (selection < 0) {
                selection = menuTexts.Length - 1;
            }
        } else if (Input.GetAxis("Vertical") < -axisThreshold && yLastFrame >= -axisThreshold) {
            selection++;
            if (selection > menuTexts.Length - 1) {
                selection = 0;
            }
        }
        yLastFrame = Input.GetAxis("Vertical");

        foreach (TMP_Text text in menuTexts) {
            text.fontSize = 48;
            text.fontStyle = FontStyles.Normal;
        }
        menuTexts[selection].fontSize = 72;
        menuTexts[selection].fontStyle = FontStyles.Bold;

        if (Input.GetButtonDown("Submit")) {
            if (selection == 0) {
                StartCoroutine(LM.loadLevel("Intro"));
            } else {
                Application.Quit();
            }
        }
    }
}
