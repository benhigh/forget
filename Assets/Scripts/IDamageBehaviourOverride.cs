public interface IDamageBehaviourOverride {
    void TakeDamage(bool death);
}