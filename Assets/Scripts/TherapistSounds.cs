﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class TherapistSounds : MonoBehaviour
{
	private AudioSource _audioSource;
	public AudioClip[] therapistNoises;
	public Random random;
    // Start is called before the first frame update
    void Start()
    {
		random = new Random();
		_audioSource = GetComponent<AudioSource>();
		_audioSource.clip = therapistNoises[random.Next(0, therapistNoises.Length)];
		StartCoroutine(waitAndPlay());
    }

    // Update is called once per frame
    void Update()
    {

    }

	IEnumerator waitAndPlay() {
		for (;;) {
			_audioSource.Play();
			yield return new WaitForSeconds(_audioSource.clip.length);
			_audioSource.clip = therapistNoises[random.Next(0, therapistNoises.Length)];
		}
	}
}
