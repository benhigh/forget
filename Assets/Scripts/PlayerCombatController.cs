﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class PlayerCombatController : MonoBehaviour {
    public int health = 5;

    private float _invincibilityTime = 0f;
    public float invincibilityMaxTime = 2f;

    private float _weaponCooldown = 0f;
    public float weaponCooldownTime = 0.5f;

    private float _weaponUpTime = 0f;
    public float weaponStayUpTime = 0.1f;

    public float knockBackAmount = 3;

    private PlayerController _controller;

    public GameObject leftAttackObject;
    public GameObject rightAttackObject;
    private PlayerSpriteController _spriteController;
    
    private Animator _rightAttackAnimator;
    private Animator _leftAttackAnimator;
    private static readonly int AttackAnimParam = Animator.StringToHash("Attack");

    private AudioSource _audioSource;
    private Rigidbody2D _rigidbody;
    public AudioClip[] spearNoises;

    private void Start() {
        _controller = GetComponent<PlayerController>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _spriteController = _controller.GetComponent<PlayerSpriteController>();

        _rightAttackAnimator = rightAttackObject.GetComponent<Animator>();
        _leftAttackAnimator = leftAttackObject.GetComponent<Animator>();

        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (_invincibilityTime <= 0f)
        {
            _spriteController.CancelInvoke();
            if (!_spriteController.visible)
            {
                _spriteController.Change();
            }
            _spriteController.invincible = false;
        }

        _weaponCooldown = Mathf.Clamp(_weaponCooldown - Time.deltaTime, 0, weaponCooldownTime);
        _invincibilityTime = Mathf.Clamp(_invincibilityTime - Time.deltaTime, 0, invincibilityMaxTime);
        _weaponUpTime = Mathf.Clamp(_weaponUpTime - Time.deltaTime, 0, weaponStayUpTime);

        if (Input.GetButtonDown("Fire1"))
        {
            if (_weaponCooldown <= 0.0f)
            {
                Attack();

                _weaponUpTime = weaponStayUpTime;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        Debug.Log(other.gameObject.tag);
        
        if ((other.gameObject.CompareTag("EnemyDamager") || other.gameObject.CompareTag("Projectile")) && _invincibilityTime <= 0) {
            Debug.Log("Hit");
            health--;
            _spriteController.invincible = true;
            _spriteController.FlickerSprite();
            _invincibilityTime = invincibilityMaxTime;

            // Either knock the player back from the direction the enemy is, relative to the player, or knock the player
            // back based on their current heading if the X values are the exact same.
            if (other.transform.position.x > transform.position.x) {
                _rigidbody.AddForce(new Vector2(-knockBackAmount, 0));
            } else if (other.transform.position.x < transform.position.x) {
                _rigidbody.AddForce(new Vector2(knockBackAmount, 0));
            } else {
                if (_controller.facing == PlayerController.Direction.Left) {
                    _rigidbody.AddForce(new Vector2(knockBackAmount, 0));
                } else {
                    _rigidbody.AddForce(new Vector2(-knockBackAmount, 0));
                }
            }
        }
    }

    private void Attack() {
        if (_controller.facing == PlayerController.Direction.Left) {
            _leftAttackAnimator.SetBool(AttackAnimParam, true);
        } else {
            _rightAttackAnimator.SetBool(AttackAnimParam, true);
        }
        
        Random random = new Random();
        _audioSource.clip = spearNoises[random.Next(0, spearNoises.Length)];
        _audioSource.Play();
    }
}
