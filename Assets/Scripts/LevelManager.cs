﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

// Author: Jason Weimann

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    //private string[] levelNames;

    private ScreenWipe screenWipe;
    private int nextLevelIndex;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        screenWipe = FindObjectOfType<ScreenWipe>();
    }

    void Update()
    {

    }

    public IEnumerator loadLevel(string sceneName)
    {
		/*
        nextLevelIndex++;
        if (nextLevelIndex >= levelNames.Length)
            nextLevelIndex = 0;

        string nextLevelName = levelNames[nextLevelIndex];
		*/

		Debug.Log("Starting screen wipe");
        screenWipe.ToggleWipe(true);
        while (!screenWipe.isDone)
            yield return null;

        Debug.Log("Done wiping screen");

		Debug.Log("Loading scene...");
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

		Debug.Log("Unwiping...");
        screenWipe.ToggleWipe(false);
    }
}
