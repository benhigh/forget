﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetector : MonoBehaviour {
    private int _groundsColliding = 0;
    
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Ground")) {
            _groundsColliding++;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.CompareTag("Ground")) {
            _groundsColliding--;
        }
    }

    public bool IsOnGround() {
        return _groundsColliding > 0;
    }
}
