﻿using System.Collections;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq.Expressions;
using UnityEditor;
using UnityEngine;

public class PlayerSpriteController : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private PlayerController _playerScript;

    public bool visible = true;
    public bool invincible = false;

    // Start is called before the first frame update
    void Start()
    {
        GameObject player = GameObject.Find("Player");
        _playerScript = player.GetComponent<PlayerController>();
        _spriteRenderer = player.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_playerScript.facing == PlayerController.Direction.Left)
        {
            _spriteRenderer.flipX = true;
        }
        else
        {
            _spriteRenderer.flipX = false;
        }
    }

    public void Change()
    {
        if (visible)
        {
            _spriteRenderer.color = new Color(1f, 1f, 1f, 0f);
            visible = false;
        }
        else
        {
            _spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
            visible = true;
        }
    }

    public void FlickerSprite()
    {
        if (invincible == true)
        {
            InvokeRepeating("Change", 0f, 0.180f);
        }
        else
        {
            CancelInvoke();
            if (!visible)
            {
                Change();
            }
        }
    }
}
