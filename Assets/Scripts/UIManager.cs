﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public RawImage[] images;
    public PlayerCombatController playerCombatController;

    private int _displayedHealth = 10;
    
    private void Update() {
        if(_displayedHealth == 0)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    
        if (playerCombatController.health != _displayedHealth) {
            _displayedHealth = playerCombatController.health;

            for (int i = 0; i < 10; i++) {
                images[i].enabled = i < _displayedHealth;
            }
        }
    }
}
