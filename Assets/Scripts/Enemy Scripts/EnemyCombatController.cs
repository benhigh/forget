﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombatController : MonoBehaviour {
    public int health = 5;
    public float knockBackAmount = 100f;

    private float _invinicibilityTime = 0.0f;
    public float maxInvincibilityTime = 1f;
    
    private Rigidbody2D _rigidbody;
    private IDamageBehaviourOverride _damageBehaviourOverride;
    
    /*private AudioSource audioPlayer;
    public AudioClip damageSound; */

    private void Start() {
        _rigidbody = GetComponent<Rigidbody2D>();
        _damageBehaviourOverride = GetComponent<IDamageBehaviourOverride>();
        // audioPlayer = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("PlayerDamager") && _invinicibilityTime <= 0.0f) {
            health--;
            // audioPlayer.PlayOneShot(damageSound);
            _damageBehaviourOverride?.TakeDamage(health == 0); // That's what this is for

            if (health == 0) {
                Destroy(gameObject);
            }
            
            _invinicibilityTime = maxInvincibilityTime;

            if (_rigidbody != null) {
                if (other.transform.position.x > transform.position.x) {
                    _rigidbody.AddForce(new Vector2(-knockBackAmount, 0));
                }
                else {
                    _rigidbody.AddForce(new Vector2(knockBackAmount, 0));
                }
            }
        }
    }

    private void Update() {
        _invinicibilityTime = Mathf.Clamp(_invinicibilityTime - Time.deltaTime, 0, maxInvincibilityTime);
    }
}
