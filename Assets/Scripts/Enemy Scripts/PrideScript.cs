﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrideScript : MonoBehaviour, IDamageBehaviourOverride {
    private Transform target;
    public float speed;
    public GameObject player;
    private Rigidbody2D _rigidbody;
    public float jumpPower;
    private bool moveAway = false;

    public GameObject enemy;
    private AudioSource _audioSource;
    public AudioClip damageSound;
    
    // Start is called before the first frame update
    void Start() {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = damageSound;
    }

    // Update is called once per frame
    void Update()
    {
        //Pride will move towards player if within range. Once it hits the player it will start moving away.
        float range = Vector2.Distance (this.transform.position, target.position);
        if (range <= 2f) {
            transform.Translate(Vector2.MoveTowards (enemy.transform.position, player.transform.position, range) * speed * Time.deltaTime);
            moveAway = true;
        }

        if (moveAway) {
            transform.Translate(Vector2.MoveTowards (enemy.transform.position, player.transform.position, range) * speed * Time.deltaTime);
        }

        if (moveAway && range >= 6f) {
            moveAway = false;
        }
        if (range <= 15f && !moveAway) {
            transform.Translate(-Vector2.MoveTowards (enemy.transform.position, player.transform.position, range) * speed * Time.deltaTime);
            //_rigidbody.AddForce(new Vector2(0, jumpPower));
        }


    }

    public void TakeDamage(bool death) {
        AudioSource.PlayClipAtPoint(damageSound, transform.position);
    }
}
 