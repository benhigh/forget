﻿using UnityEngine;

public class LustSpawner : MonoBehaviour {
    public float delay = 6.0f;
    public float lustLength = -50;
    public bool goingRight = true;
    public GameObject lust;
    
    private float _delayLeft = 0;

    private void Update() {
        _delayLeft += Time.deltaTime;
        if (_delayLeft >= delay) {
            _delayLeft = 0;
            GameObject thisLust = Instantiate(lust);
            thisLust.GetComponent<LustScript>().despawnX = lustLength;
            thisLust.GetComponent<LustScript>().goingRight = goingRight;
            thisLust.transform.position = transform.position;
        }
    }
}
