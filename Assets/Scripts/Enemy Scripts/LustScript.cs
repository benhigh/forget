﻿using UnityEngine;
using UnityEngine.Serialization;
using Random = System.Random;

public class LustScript : MonoBehaviour, IDamageBehaviourOverride {
    public float speed = 3;
    public float arcHeight = 3;
    public float despawnX = 25;
    public float vertMod = 0.7f;

    private float _arcPeakHeight;

    private AudioSource _audioSource;
    public AudioClip damageClip;

    [FormerlySerializedAs("_goingRight")] public bool goingRight = true;
    private bool _goingDown = true;
    
    private void Start() {
        _arcPeakHeight = transform.position.y;
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = damageClip;
    }

    void Update() {
        Vector2 direction = new Vector2();

        direction.x = goingRight ? speed : -speed;
        direction.y = _goingDown ? -speed * vertMod : speed * vertMod;
        
        // Change direction when at the peak
        if (_goingDown && transform.position.y <= _arcPeakHeight - arcHeight) {
            _goingDown = false;
        } else if (!_goingDown && transform.position.y >= _arcPeakHeight) {
            _goingDown = true;
        }
        
        transform.Translate(direction);

        if (transform.position.x >= despawnX) {
            Destroy(gameObject);
        }
    }

    public void TakeDamage(bool death) {
        AudioSource.PlayClipAtPoint(damageClip, transform.position);
    }
}
