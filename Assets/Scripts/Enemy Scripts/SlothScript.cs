﻿using UnityEngine;

public class SlothScript : MonoBehaviour, IDamageBehaviourOverride {
    public Transform playerTransform;
    public float xDistanceAway = 20;
    public float yDistanceAway = 2;
    public float rollForce = 150;
    
    private Rigidbody2D _rigidbody;

    public AudioClip damageClip;

    private void Start() {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if (Mathf.Abs(playerTransform.position.x - transform.position.x) <= xDistanceAway &&
            Mathf.Abs(playerTransform.position.y - transform.position.y) <= yDistanceAway) {

            float xDirection = playerTransform.position.x >= transform.position.x ? 1 : -1;
            _rigidbody.AddForceAtPosition(new Vector2(rollForce * xDirection, 0), new Vector2(0, 0.2f));
        }
    }

    public void TakeDamage(bool death) {
        _rigidbody.velocity = Vector3.zero;
        AudioSource.PlayClipAtPoint(damageClip, transform.position);
    }
}
