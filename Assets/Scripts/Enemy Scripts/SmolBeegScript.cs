﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmolBeegScript : MonoBehaviour, IDamageBehaviourOverride {
    public int moveSpeed;
    private Transform target;
    private int _jumpDir;
    private Rigidbody2D _rigidbody;
    private bool firstJump = false;

    public AudioClip damageClip;
    // Start is called before the first frame update
    void Start() {
        _rigidbody = this.GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        BeegJump();
    }

    void BeegJump() {
        if (target.position.x > 0 ) {
            _jumpDir = -1;
        }
        else {
            _jumpDir = 1;
        }
            
        transform.Translate(moveSpeed * Time.deltaTime * _jumpDir, 0, 0);
        _rigidbody.AddForce(Vector2.up * 30, ForceMode2D.Impulse);
 

    }
    
    // Update is called once per frame
    void Update()
    {
        //BeegJump();

    }

    public void TakeDamage(bool death) {
        AudioSource.PlayClipAtPoint(damageClip, transform.position);
    }
}
