﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GluttonyScript : MonoBehaviour, IDamageBehaviourOverride {
    private Transform target;

    public float speed;
    private float xDirection;
    private Rigidbody2D _rigidbody;
    // Start is called before the first frame update

    private AudioSource _audioSource;
    public AudioClip damageClip;
    
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        _rigidbody = this.GetComponent<Rigidbody2D>();
        float xDirection = target.position.x >= transform.position.x ? 1 : -1;
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = damageClip;
    }

    void CheckXDirection() {
        float xDirection = target.position.x >= transform.position.x ? 1 : -1;
    }
    
    // Update is called once per frame
    void Update()
    {
       
        float range = Vector2.Distance (this.transform.position, target.position);
        if (range <= 10) {
            float xDirection = target.position.x >= transform.position.x ? 1 : -1;
            _rigidbody.AddForce(new Vector2(xDirection*speed, 0));
        }
            
        

    }

    public void TakeDamage(bool death) {
        Debug.Log("Gluttony damaged");
        AudioSource.PlayClipAtPoint(damageClip, transform.position);
    }
}
