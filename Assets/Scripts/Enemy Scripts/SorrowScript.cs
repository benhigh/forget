﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SorrowScript : MonoBehaviour {
    private bool isDead;
    private bool imSad;
    private AudioSource audioPlayer;
    public AudioClip deathSound;
    public int waitForHub;

    public LevelManager LM;

  	void Awake() {
  		LM = FindObjectOfType<LevelManager>();
  	}

    void Start() {
        audioPlayer = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D col) {
        if (col.transform.tag == "PlayerDamager") {
            StartCoroutine(returnToHub());

            PlayerController.pictureProgress++;
        }

    }

    private IEnumerator returnToHub() {

        audioPlayer.PlayOneShot(deathSound);
        gameObject.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(waitForHub);
        StartCoroutine(LM.loadLevel("Hub"));
    }
}
